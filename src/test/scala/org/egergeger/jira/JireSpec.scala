package org.egergeger.jira

import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.PageFactory
import org.scalatest.concurrent.{IntegrationPatience, Eventually}
import org.scalatest.selenium.Firefox
import org.scalatest.{BeforeAndAfterAll, ShouldMatchers, FunSpec}
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator
import scala.reflect.runtime.universe._

abstract class JireSpec extends FunSpec with Firefox with ShouldMatchers with BeforeAndAfterAll
with Eventually with IntegrationPatience {

  private val _runtimeMirror = runtimeMirror(getClass.getClassLoader)

  protected def open[T <: Page](action: T => Unit = null)(implicit tt: TypeTag[T], driver: WebDriver): T = {
    val page: T = on[T](null)(tt, driver)
    goTo(page)

    if (action != null) action.apply(page)

    page
  }

  protected def on[T](action: T => Unit = null)(implicit tt: TypeTag[T], driver: WebDriver): T = {
    val page = _runtimeMirror
      .reflectClass(tt.tpe.typeSymbol.asClass)
      .reflectConstructor(tt.tpe.declaration(nme.CONSTRUCTOR).asMethod)
      .apply().asInstanceOf[T]

    PageFactory.initElements(new HtmlElementDecorator(driver), page)

    if(action != null) action.apply(page)

    page
  }

  override protected def afterAll(): Unit = {
    webDriver.quit()
    super.afterAll()
  }
}
