package org.egergeger.jira

import com.jayway.awaitility.Awaitility._
import com.jayway.awaitility.Duration
import java.lang.Boolean
import java.util.concurrent.Callable
import scala.util.Random

class IssueSpec extends JireSpec with Settings {

  describe("Jira") {

    it("should create issue") {
      val issueSummary = Random.nextString(10)

      open[DashboardPage](p => {
        p.createIssue(dlg => {
          dlg.summary = issueSummary
          dlg.submit()
        })
      })

      eventually({pageSource should include(issueSummary)})
    }

    it("should update issue") {
      val issueSummary = Random.nextString(10)
      val updatedIssueSummary = Random.nextString(10)

      open[DashboardPage](p => {
        p.createIssue(dlg => {
          dlg.summary = issueSummary
          dlg.submit()
        })

        p.openCreatedIssue(issueSummary)
      })

      on[IssuePage]().editIssue(dlg => {
        dlg.summary = updatedIssueSummary
        dlg.submit()
      })

      eventually({pageSource should include(updatedIssueSummary)})
    }

    it("should find issue") {
      val issueSummary = Random.nextString(10)

      open[DashboardPage](p => {
        p.createIssue(dlg => {
          dlg.summary = issueSummary
          dlg.submit()
        })

        p.search(issueSummary)
      })

      eventually({pageSource should include(issueSummary)})
    }

  }

  override protected def beforeAll(): Unit = {
    open[DashboardPage]().goToSignIn()

    on[SignInPage]().signIn(username, password)

    await.atMost(Duration.ONE_MINUTE).until(new Callable[Boolean] {
      override def call(): Boolean = currentUrl.contains("jira.atlassian.com")
    })

    super.beforeAll()
  }
}
