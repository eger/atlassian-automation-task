package org.egergeger.jira

import com.jayway.awaitility.Awaitility._
import java.lang.Boolean
import java.util.concurrent.{Callable, TimeUnit}
import org.openqa.selenium.support.FindBy
import org.scalatest.selenium.Page
import ru.yandex.qatools.htmlelements.element.{TextInput, Button}
import org.openqa.selenium.{By, WebDriver}


class DashboardPage extends Page {

  override val url: String = "https://jira.atlassian.com/secure/Dashboard.jspa"

  @FindBy(css = "a[href*='login']")
  private var loginBtn: Button = null

  @FindBy(id = "create_link")
  private var createIssueBtn: Button = null

  @FindBy(id = "create-issue-dialog")
  private var createIssueDlg: IssueDialog = null

  @FindBy(id = "quickSearchInput")
  private var searchTxt: TextInput = null

  def goToSignIn() = loginBtn.click()

  def createIssue(action: IssueDialog => Unit) {
    createIssueBtn.click()

    await.atMost(15, TimeUnit.SECONDS).until(new Callable[Boolean] {
      override def call(): Boolean = createIssueDlg.isDisplayed
    })

    action.apply(createIssueDlg)
  }

  def search(str:String)(implicit wd:WebDriver){
    searchTxt.sendKeys(str)
    searchTxt.getWrappedElement.submit()

    await.atMost(15, TimeUnit.SECONDS).until(new Callable[Boolean] {
      override def call(): Boolean = wd.getCurrentUrl.contains("browse")
    })
  }

  def openCreatedIssue(issue:String)(implicit webDriver:WebDriver){
    webDriver.findElement(By.partialLinkText(issue)).click()
  }

}
