package org.egergeger.jira

import com.jayway.awaitility.Awaitility._
import java.lang.Boolean
import java.util.concurrent.{Callable, TimeUnit}
import org.openqa.selenium.support.FindBy
import ru.yandex.qatools.htmlelements.element.{Button, TextInput, HtmlElement}


class IssueDialog extends HtmlElement {

  @FindBy(id = "summary")
  private var summaryTxt: TextInput = null

  @FindBy(css = "[id*='issue-submit']")
  private var submitIssueBtn: Button = null

  def summary = summaryTxt.getText

  def summary_=(value: String): Unit = summaryTxt.sendKeys(value)

  override def submit() = {
    submitIssueBtn.click()

    await.atMost(15, TimeUnit.SECONDS).until(new Callable[Boolean] {
      override def call(): Boolean = try {
        !isDisplayed
      } catch {
        case _: Throwable => true
      }
    })
  }

}
