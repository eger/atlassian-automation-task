package org.egergeger.jira

import ru.yandex.qatools.htmlelements.element.Button
import org.openqa.selenium.support.FindBy
import com.jayway.awaitility.Awaitility._
import java.util.concurrent.{Callable, TimeUnit}
import java.lang.Boolean

class IssuePage {

  @FindBy(id = "edit-issue")
  private var editIssueBtn: Button = null

  @FindBy(id = "edit-issue-dialog")
  private var issueEditDialog: IssueDialog = null

  def editIssue(action:IssueDialog => Unit){
    editIssueBtn.click()

    await.atMost(10, TimeUnit.SECONDS).until(new Callable[Boolean] {
      override def call(): Boolean = issueEditDialog.isDisplayed
    })

    action.apply(issueEditDialog)
  }

}
