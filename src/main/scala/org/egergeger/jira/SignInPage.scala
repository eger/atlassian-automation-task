package org.egergeger.jira

import org.openqa.selenium.support.FindBy
import org.scalatest.selenium.Page
import ru.yandex.qatools.htmlelements.element.{Button, TextInput}

class SignInPage extends Page {

  override val url: String = "https://id.atlassian.com/login"

  @FindBy(id = "username")
  private var usernameTxt: TextInput = null

  @FindBy(id = "password")
  private var passwordTxt: TextInput = null

  @FindBy(id = "login-submit")
  private var submitBtn: Button = null

  def signIn(username: String, password: String) {
    usernameTxt.sendKeys(username)
    passwordTxt.sendKeys(password)
    submitBtn.click()
  }

}
